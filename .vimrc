syntax enable 
set mouse=a                           "allow mouse clicks to change cursor position
filetype plugin on
set number
set relativenumber
set noswapfile
set nowrap                            " Don't wrap long lines
set autoindent
set smartindent
set incsearch	                       "search using / and highlight
set ignorecase                        "case insensitive
"set timeout timeoutlen=1500
set timeoutlen=1000 ttimeoutlen=0     " Remove timeout when hitting escape
set ruler                             " show the cursor position all the time
set splitright                        " Open new splits to the right
set splitbelow                        " Open new splits to the bottom
set noerrorbells novisualbell         " Turn off visual and audible bells
set expandtab shiftwidth=2 tabstop=2  " Two spaces for tabs everywhere
set backspace=2                       " Backspace deletes like most programs in insert mode
set showcmd                           " display incomplete commands
" Leader Mappings
map <Space> <leader>
map <Leader>w :update<CR>
map <Leader>q :qall<CR>

setlocal spell spelllang=en_us
"Copy and paste
set clipboard=unnamedplus
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+
"---------
map <C-n> :NERDTreeToggle<CR>

"Theme:
set t_Co=256
let g:gruvbox_termcolors='256'
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_guisp_fallback='bg'
set background=dark	"solarized or gruvbox
colorscheme gruvbox    "theme name

"Spelling errors higlight
"fg stands for foreground and letters color, bg stands for background (doesn't work with therming).
hi SpellBad cterm=underline ctermfg=NONE ctermbg=NONE 
" Autocomplete with dictionary words when spell check is on
set complete+=kspell

"Make the 81st column stand out
"highlight ColorColumn ctermbg=0 guibg=lightgray
"set colorcolumn=81
set textwidth=80
set colorcolumn=+1

"Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**
" Display all matching files when we tab complete
set wildmenu                          " Tab autocomplete in command mode
"set wildmode=list:longest,list:full
" NOW WE CAN:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy

"auto-complete PHP
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
set completeopt=menuone,longest,preview
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Always use vertical diffs
set diffopt+=vertical

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Get off my lawn
nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>

